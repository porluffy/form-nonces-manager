<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Forbidden' ); }
/**
 * Plugin Name: Form Nonces Manager
 * Plugin URI: http://hlaporthein.me
 * Description: Hla Por Thein - WordPress Nonce OOP For Assignment
 * Version: 1.0
 * Author: Hla Por Thein
 * Author URI: http://hlaporthein.me
 * License: GPL2+
 * Text Domain: fnm
 */



require plugin_dir_path(__FILE__) . 'demo/plugin-functionality.php';

/**
 * check composer is loaded or not
 */

if ( file_exists( plugin_dir_path(__FILE__) . 'vendor/autoload.php' ) ) {

    require plugin_dir_path(__FILE__) . 'vendor/autoload.php';
    require plugin_dir_path(__FILE__) . 'demo/subscribe-email-widget.php';

} else {

    /**
     * Show message to install composer
     */
    add_action('wp_footer', function(){
        echo fnm_show_message("Need to install composer in form nonce manager plugin.");
    });

}