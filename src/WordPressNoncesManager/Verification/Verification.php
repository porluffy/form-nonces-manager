<?php

namespace WordPressNoncesManager\Verification;



use WordPressNoncesManager\Configuration\NonceConfiguration;
use WordPressNoncesManager\Nonces\NonceAbstract;

/**
 * Nonce Verification Process
 *
 * Class Verification
 * @package WordPressNoncesManager\Verification
 */
class Verification extends NonceAbstract implements VerificationInterface
{

    /**
     * Verification constructor.
     * @param NonceConfiguration $nonceConfiguration
     */
    public function __construct(NonceConfiguration $nonceConfiguration)
    {
        $this->setAction($nonceConfiguration->getAction());
        $this->setRequestName($nonceConfiguration->getRequestName());
        $this->setLifetime($nonceConfiguration->getLifetime());

        // If the $_REQUEST is set, we set the nonce here.
        if (isset($_REQUEST[$this->getRequestName()])) {
            $nonce = sanitize_text_field(wp_unslash($_REQUEST[$this->getRequestName()]));
            $this->setNonce($nonce);
        }
    }

    /**
     * {@inheritDoc}
     **/
    public function verify(string $nonce = null): bool
    {
        if ($nonce !== null) {
            $this->setNonce($nonce);
        }

        $valid = $this->getAge();

        return (bool) $valid;
    }

    /**
     * {@inheritDoc}
     **/
    public function getAge(): string
    {
        return (string) wp_verify_nonce($this->getNonce(), $this->getAction());
    }

    /**
     * {@inheritDoc}
     **/
    public function showMessageHasExpired(string $action = null): void
    {
        if ($action !== null) {
            $this->setAction($action);
        }

        wp_nonce_ays($this->getAction());
    }

    /**
     * Verification process for admin aera
     *
     * @return bool
     */
    public function isAdminReferer(): bool
    {
        return (bool) check_admin_referer($this->getAction(), $this->getRequestName());
    }

    /**
     * Verify nonce is passed in ajax request
     *
     * @param bool $die
     * @return bool
     */
    public function isAjaxReferer(bool $die = false): bool
    {
        return (bool) check_ajax_referer($this->getAction(), $this->getRequestName(), $die );
    }
}