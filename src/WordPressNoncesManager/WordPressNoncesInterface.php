<?php

namespace WordPressNoncesManager;


use WordPressNoncesManager\Configuration\NonceConfiguration;
use WordPressNoncesManager\Nonces\Nonce;
use WordPressNoncesManager\Types\FieldType;
use WordPressNoncesManager\Types\UrlType;
use WordPressNoncesManager\Verification\Verification;

/**
 * Interface WordPressNoncesInterface
 * @package WordPressNoncesManager
 */
interface WordPressNoncesInterface
{


    public function configuration(): NonceConfiguration;

    /**
     * Returns the nonce create API to create a new nonce
     *
     * @return Nonce
     */
    public function nonce(): Nonce;

    /**
     * Returns the nonce create field API to create a new nonce field
     *
     * @return FieldType
     */
    public function field(): FieldType;

    /**
     * Returns the nonce create url API to create a new nonce url
     *
     * @return UrlType
     */
    public function url(): UrlType;


    /**
     * Verify none from form
     *
     * @return Verification
     */
    public function verify(): Verification;
}