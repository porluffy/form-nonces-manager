<?php

namespace WordPressNoncesManager;

use WordPressNoncesManager\Configuration\NonceConfiguration;
use WordPressNoncesManager\Nonces\Nonce;
use WordPressNoncesManager\Types\FieldType;
use WordPressNoncesManager\Types\UrlType;
use WordPressNoncesManager\Verification\Verification;


/**
 * Class WordPressNoncesManager
 * @package WordPressNoncesManager
 */
final class WordPressNoncesManager implements WordPressNoncesInterface
{
    /**
     * @var NonceConfiguration
     */
    private $nonceConfiguration;

    /**
     * The Nonce
     *
     * @access private
     * @var Nonce
     */
    private $nonce;

    /**
     * The Nonce Field
     *
     * @access private
     * @var FieldType
     */
    private $field;

    /**
     * The Nonce Url
     *
     * @access private
     * @var UrlType
     */
    private $url;

    /**
     * The Nonce Verification
     *
     * @access private
     * @var Verification
     */
    private $verification;

    /**
     * WordPressNoncesManager constructor.
     *
     * @param NonceConfiguration $nonceConfiguration
     * @param FieldType|null $fieldType
     * @param UrlType|null $urlType
     * @param Verification|null $verification
     */
    private function __construct(NonceConfiguration $nonceConfiguration, FieldType $fieldType = null, UrlType $urlType = null, Verification $verification = null)
    {
        $this->nonceConfiguration = $nonceConfiguration;

        if (!isset($fieldType, $urlType, $verification)) {
            $this->registerComponents($nonceConfiguration);
        } else {
            $this->field = $fieldType;
            $this->url = $urlType;
            $this->verification = $verification;
        }

        $this->nonce = new Nonce($nonceConfiguration);
    }

    /**
     * Create nonces
     *
     * @param NonceConfiguration $nonceConfiguration
     * @param FieldType $fieldType
     * @param UrlType $urlType
     * @param Verification $verification
     * @return WordPressNoncesManager
     */
    public static function create(NonceConfiguration $nonceConfiguration, FieldType $fieldType, UrlType $urlType, Verification $verification): WordPressNoncesManager
    {
        return new WordPressNoncesManager($nonceConfiguration, $fieldType, $urlType, $verification);
    }

    /**
     * Dependency inject for wordpress nonces
     *
     * @param NonceConfiguration $nonceConfiguration
     * @return WordPressNoncesManager
     */
    public static function build(NonceConfiguration $nonceConfiguration): WordPressNoncesManager {
        return new WordPressNoncesManager($nonceConfiguration);
    }

    /**
     * Register Nonce Configuration
     *
     * @param NonceConfiguration $nonceConfiguration
     */
    private function registerComponents(NonceConfiguration $nonceConfiguration): void
    {
        $this->field = new FieldType($nonceConfiguration);
        $this->url = new UrlType($nonceConfiguration);
        $this->verification = new Verification($nonceConfiguration);

    }

    /**
     * Get nonce configuration
     * @return NonceConfiguration
     */
    public function configuration(): NonceConfiguration
    {
        return $this->nonceConfiguration;
    }

    /**
     * Get nonce
     *
     * @return Nonce
     */
    public function nonce(): Nonce {
        return $this->nonce;
    }

    /**
     * Get nonce field
     *
     * @return FieldType
     */
    public function field(): FieldType
    {
        return $this->field;
    }

    /**
     * Get nonce url
     *
     * @return UrlType
     */
    public function url(): UrlType
    {
        return $this->url;
    }

    /**
     * get nonce verification
     *
     * @return Verification
     */
    public function verify(): Verification
    {
        return $this->verification;
    }
}