<?php


namespace WordPressNoncesManager\Nonces;

/**
 * Interface NonceInterface
 * @package WordPressNoncesManager\Nonces
 */
interface NonceInterface
{
    /**
     * Create a new nonce
     *
     * @link https://codex.wordpress.org/Function_Reference/wp_create_nonce
     *
     * @return void
     **/
//    public function create(): void;
}