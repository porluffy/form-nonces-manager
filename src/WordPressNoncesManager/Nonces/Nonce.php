<?php

namespace WordPressNoncesManager\Nonces;


use WordPressNoncesManager\Configuration\NonceConfiguration;

/**
 * Class Nonce
 * @package WordPressNoncesManager\Nonces
 */
class Nonce extends NonceAbstract implements NonceInterface
{

    public function __construct(NonceConfiguration $nonceConfiguration)
    {
        $this->setAction($nonceConfiguration->getAction());
        $this->setRequestName($nonceConfiguration->getRequestName());
        $this->setLifetime($nonceConfiguration->getLifetime());
    }

    /**
     * {@inheritDoc}
     */
    public function create(): void
    {
        $this->setNonce(wp_create_nonce($this->getAction()));
    }

    /**
     * Check if Nonce is setted or not
     *
     * @return bool     true if is setted | false if isn't setted
     */
    protected function check(): bool {
        $nonce = $this->getNonce();

        return ($nonce !== null || $nonce !== '');
    }
}