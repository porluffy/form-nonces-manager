<?php

use WordPressNoncesManager\Configuration\NonceConfiguration;
use WordPressNoncesManager\WordPressNoncesManager;


/**
 * Class WordPressNoncesHelpers
 */
class  WordPressNoncesHelpers {

    private $configuration;


    private $wordpressNoncesManager;


    function __construct(string  $action, string $name, int $lifetime = 30)
    {
        $this->configuration = new  NonceConfiguration($action, $name, $lifetime);
        $this->wordpressNoncesManager = WordPressNoncesManager::build($this->configuration);
    }

    /**
     * Get Nonce value
     *
     * @return string
     */
    public function getNonce() : string
    {
        $this->wordpressNoncesManager->nonce()->create();

        return $this->wordpressNoncesManager->nonce()->getNonce();
    }


    /**
     * Get nonce link url
     *
     * @param string $link
     * @return string
     */
    public function generateWithLink(string $link) : string
    {
        return $this->wordpressNoncesManager->url()->generate($link);
    }


    /**
     * Get nonce field input
     *
     * @return string
     */
    public function getField() : string
    {
        return $this->wordpressNoncesManager->Field()->generate();
    }


    /**
     * Get nonce verify
     *
     * @return bool
     */
    public function verify() : bool
    {
        $verification = $this->wordpressNoncesManager->verify();
        return  $verification->verify();
    }

}


/**
 * verification of nonce
 *
 * @param string $action
 * @param string $name
 * @param int $lifetime
 * @return bool
 */
function fnm_nonce_verify(string $action, string $name, $lifetime = 30) : bool {
    return (new WordPressNoncesHelpers($action, $name, $lifetime))->verify();
}


/**
 * Create nonce field
 *
 * @param string $action
 * @param string $name
 * @param int $lifetime
 * @return string
 */
function fnm_nonce_field(string $action, string $name, $lifetime = 30) : string {
    return (new WordPressNoncesHelpers($action, $name, $lifetime))->getField();
}

/**
 * Create nonce values
 *
 * @param string $action
 * @param string $name
 * @param int $lifetime
 * @return string
 */
function fnm_create_nonce(string $action, string $name, $lifetime = 30) : string {
        return (new WordPressNoncesHelpers($action, $name, $lifetime))->getNonce();
}


/**
 * get nonce url
 *
 * @param string $action
 * @param string $name
 * @param string $link
 * @param int $lifetime
 * @return string
 */
function fnm_nonce_url(string $action, string $name, string $link, $lifetime = 30) : string {
    return (new WordPressNoncesHelpers($action, $name, $lifetime))->generateWithLink($link);
}