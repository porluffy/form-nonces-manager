<?php

namespace Tests;

use Brain\Monkey;
use Brain\Monkey\Functions;
use PHPUnit\Framework\TestCase;



use function defined;
use function define;

/**
 * Class AbstractTestCase
 * @package tests
 */
abstract class AbstractTestCase extends TestCase
{

    /**
     * Define the used standard Wordpress Time Interval
     */
    private function defineDefaultTimeInterval(): void {
        if (!defined('DAY_IN_SECONDS')) {
            define('DAY_IN_SECONDS', 86400);
        }
    }

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     **/
    protected function setUp(): void
    {
        $this->defineDefaultTimeInterval();

        parent::setUp();
        Monkey::setUpWP();

        // We mock wp_nonce_url.
        Functions::expect('wp_nonce_url')->andReturnUsing(function ($url, $action, $request_name) {
            return $url . $action . $request_name;
        });

        // we mock wp_create_nonce with sha1().
        Functions::when('wp_create_nonce')->alias('sha1');

        // we mock wp_verify_nonce.
        Functions::expect('wp_verify_nonce')->andReturnUsing(function ($nonce, $action) {
            return sha1($action) === $nonce;
        });

        // we mock wp_unslash.
        Functions::expect('wp_unslash')->andReturnUsing(function ($string) {
            return $string;
        });

        // we mock sanitize_text_field.
        Functions::expect('sanitize_text_field')->andReturnUsing(function ($string) {
            return $string;
        });

    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     **/
    public function tearDown(): void {
        Monkey::tearDownWP();
        parent::tearDown();
    }
}