<?php

namespace Tests\Unit;


use Brain\Monkey\WP\Filters;
use Tests\AbstractTestCase;
use WordPressNoncesManager\Configuration\NonceConfiguration;
use WordPressNoncesManager\Nonces\Nonce;
use WordPressNoncesManager\Types\FieldType;
use WordPressNoncesManager\Types\UrlType;
use WordPressNoncesManager\Verification\Verification;
use WordPressNoncesManager\WordPressNoncesManager;

class WordPressNoncesManagerTest extends AbstractTestCase
{
    /**
     * The Request Name.
     *
     * @var string
     **/
    public $request;

    /**
     * The Action.
     *
     * @var string
     **/
    public $action;

    /**
     * The Lifetime.
     *
     * @var int
     **/
    public $lifetime;

    /**
     * @var NonceConfiguration
     */
    public $configuration;

    /**
     * The WordPressNoncesManager
     *
     * @var WordPressNoncesManager
     */
    private $WordPressNoncesManager;

    /**
     * @var FieldType
     */
    private $fieldType;

    /**
     * @var UrlType
     */
    private $urlType;

    /**
     * @var Verification
     */
    private $verifier;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function setUp() : void
    {
        parent::setUp();

        $this->action = 'action';
        $this->request = 'request';
        $this->lifetime = 365;

        // The filter should be added once.
        Filters::expectAdded('nonce_life')->once();

        $this->configuration = new NonceConfiguration($this->action, $this->request, $this->lifetime);

        $this->fieldType = new FieldType($this->configuration);
        $this->urlType = new UrlType($this->configuration);
        $this->verifier = new Verification($this->configuration);
    }

    /**
     * Test create a new Nonce Manager through the static create method and inject the required Dependencies
     */
    public function testCreatedWordPressNoncesManager(): void {

        $this->WordPressNoncesManager = WordPressNoncesManager::create($this->configuration, $this->fieldType, $this->urlType, $this->verifier);

        self::assertInstanceOf(WordPressNoncesManager::class, $this->WordPressNoncesManager);
    }

    /**
     * Test the created Nonce Manager and test the inject Dependencies
     */
    public function testServicesAreInjected(): void {
        $this->WordPressNoncesManager = WordPressNoncesManager::create($this->configuration, $this->fieldType, $this->urlType, $this->verifier);

        self::assertInstanceOf(NonceConfiguration::class, $this->WordPressNoncesManager->configuration());
        self::assertInstanceOf(Nonce::class, $this->WordPressNoncesManager->nonce());
        self::assertInstanceOf(FieldType::class, $this->WordPressNoncesManager->field());
        self::assertInstanceOf(UrlType::class, $this->WordPressNoncesManager->url());
        self::assertInstanceOf(Verification::class, $this->WordPressNoncesManager->verify());
    }
}