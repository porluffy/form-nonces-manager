<?php

namespace Tests\Unit;

use Brain\Monkey\Functions;
use Tests\AbstractTestCase;
use WordPressNoncesManager\Configuration\NonceConfiguration;
use WordPressNoncesManager\Nonces\Nonce;
use WordPressNoncesManager\Verification\Verification;

/**
 * Class VerificationTest
 * @package Tests\Unit
 */
class VerificationTest extends AbstractTestCase
{

    /**
     * The request name.
     *
     * @var string
     **/
    public $request;

    /**
     * The action.
     *
     * @var string
     **/
    public $action;

    /**
     * The lifetime.
     *
     * @var int
     **/
    public $lifetime;

    /**
     * The configuration.
     *
     **/
    public $configuration;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     **/
    public function setUp(): void
    {
        parent::setUp();

        $this->action = 'action';
        $this->request = 'request';
        $this->lifetime = 213;
        $this->configuration = new NonceConfiguration($this->action, $this->request, $this->lifetime);
    }

    /**
     * Check validate
     */
    public function testValidity(): void
    {
        $create = new Nonce($this->configuration);
        $create->create();
        $nonce = $create->getNonce();

        $verify = new Verification($this->configuration);
        $valid = $verify->verify($nonce);

        // Check if nonce is valid.
        self::assertTrue($valid);

        // Check if nonce is not valid.
        $not_valid = $verify->verify('not-valid' . $nonce);
        self::assertFalse($not_valid);

        // Check auto-nonce assignment.
        $_REQUEST[$this->request] = $nonce;
        $verify = new Verification($this->configuration);
        $valid = $verify->verify();
        self::assertTrue($valid);
    }

    /**
     * Test Get Age
     *
     */
    public function testAge(): void
    {
        self::markTestSkipped('Test skip');

        $create = new Nonce($this->configuration);
        $create->create();

        $verify = new Verification($this->configuration);
        $age = $verify->getAge();

        self::assertSame(1, $age);
    }

    /**
     * Test check Admin Referer valid
     */
    public function testCheckAdminRefererValid(): void {
        $create = new Nonce($this->configuration);
        $create->create();

        // mock check_admin_referer success
        Functions::expect('check_admin_referer')
            ->once()
            ->with($this->action, $this->request)
            ->andReturn(true);

        $verify = new Verification($this->configuration);
        $valid = $verify->isAdminReferer();

        // Check if nonce is valid.
        self::assertTrue($valid);
    }

    /**
     * Test check Admin Referer invalid
     */
    public function testCheckAdminRefererInvalid(): void {
        $create = new Nonce($this->configuration);
        $create->create();

        // mock check_admin_referer success
        Functions::expect('check_admin_referer')
            ->once()
            ->with($this->action, $this->request)
            ->andReturn(false);

        $verify = new Verification($this->configuration);
        $valid = $verify->isAdminReferer();

        // Check if nonce is invalid.
        self::assertFalse($valid);
    }

    /**
     * Test check Ajax Referer valid
     */
    public function testCheckAjaxRefererValid(): void {
        $create = new Nonce($this->configuration);
        $create->create();

        // mock check_admin_referer success
        Functions::expect('check_ajax_referer')
            ->once()
            ->with($this->action, $this->request, false)
            ->andReturn(true);

        $verify = new Verification($this->configuration);
        $valid = $verify->isAjaxReferer();

        // Check if nonce is valid.
        self::assertTrue($valid);
    }

    /**
     * Test check Ajax Referer valid
     *
     */
    public function testCheckAjaxRefererInvalid(): void {
        $create = new Nonce($this->configuration);
        $create->create();

        // mock check_admin_referer success
        Functions::expect('check_ajax_referer')
            ->once()
            ->with($this->action, $this->request, false)
            ->andReturn(false);

        $verify = new Verification($this->configuration);
        $valid = $verify->isAjaxReferer();

        // Check if nonce is valid.
        self::assertFalse($valid);
    }
}