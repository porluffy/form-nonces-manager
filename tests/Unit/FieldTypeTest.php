<?php
namespace Tests\Unit;

use Brain\Monkey\Functions;
use Tests\AbstractTestCase;
use WordPressNoncesManager\Configuration\NonceConfiguration;
use WordPressNoncesManager\Types\FieldType;


/**
 * Class FieldTypeTest
 * @package Tests\Unit
 */
class FieldTypeTest extends AbstractTestCase
{

    /**
     * The action.
     *
     * @var string
     **/
    public $action;

    /**
     * The request name.
     *
     * @var string
     **/
    public $request;

    /**
     * The lifetime.
     *
     * @var int
     **/
    public $lifetime;

    /**
     * The configuration.
     *
     * @var Configuration
     **/
    public $configuration;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     **/
    public function setUp(): void
    {
        parent::setUp();

        // we mock wp_create_nonce with sha1().
        Functions::when('wp_create_nonce')->alias('sha1');

        // we mock wp_nonce_field.
        Functions::expect('wp_nonce_field')->andReturnUsing(function ($action, $request_name, $referer, $echo) {
            $string = $action . $request_name;
            if ($referer) {
                $string .= 'referer';
            }

            // Should never be true, since we call wp_nonce_field with $echo false always and do echo by ourselfs.
            if ($echo) {
                $string .= 'echo';
            }

            return $string;
        });

        // we mock wp_kses.
        Functions::expect('wp_kses')->andReturnUsing(function ($string, $array) {
            return $string;
        });

        $this->action = 'action';
        $this->request = 'request';
        $this->lifetime = 213;
        $this->configuration = new NonceConfiguration($this->action, $this->request, $this->lifetime);
    }

    /**
     * Test Field generation & output
     */
    public function testGenerateNonceField(): void
    {
        $create = new FieldType($this->configuration);
        $create->generate();
        $field = $create->getField();

        self::assertSame($field, $this->action . $this->request);

        $field = $create->generate(true);
        self::assertSame($field, $this->action . $this->request . 'referer');

        // Test echo.
        ob_start();
        $create->generate(false, true);
        $field = $create->getField();
        $echo_output = ob_get_contents();
        ob_end_clean();

        self::assertSame($echo_output, $this->action . $this->request . 'echo');
        self::assertSame($echo_output, $field);
    }
}