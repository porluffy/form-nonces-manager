<?php
namespace Tests\Unit;



use Tests\AbstractTestCase;
use WordPressNoncesManager\Configuration\NonceConfiguration;
use WordPressNoncesManager\Types\UrlType;

/**
 * Class UrlTypeTest
 * @package Tests\Unit
 */
class UrlTypeTest extends AbstractTestCase
{

    /**
     * The request name.
     *
     * @var string
     **/
    public $request;

    /**
     * The action.
     *
     * @var string
     **/
    public $action;

    /**
     * The lifetime.
     *
     * @var int
     **/
    public $lifetime;

    /**
     * The configuration.
     *
     * @var Configuration
     **/
    public $configuration;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     **/
    public function setUp(): void
    {
        parent::setUp();

        $this->action = 'action';
        $this->request = 'request';
        $this->lifetime = 213;
        $this->configuration = new NonceConfiguration($this->action, $this->request, $this->lifetime);
    }

    /**
     * Test URL generation
     */
    public function testCreateNonceURL(): void
    {
        $create = new UrlType($this->configuration);
        $url = 'http://example.com/';
        $url_with_nonce = $create->generate($url);

        self::assertSame($url_with_nonce, $url . $this->action . $this->request);
    }

}