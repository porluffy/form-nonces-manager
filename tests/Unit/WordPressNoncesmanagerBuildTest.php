<?php
namespace Tests\Unit;

use Brain\Monkey\WP\Filters;
use Tests\AbstractTestCase;
use WordPressNoncesManager\Configuration\NonceConfiguration;
use WordPressNoncesManager\Nonces\Nonce;
use WordPressNoncesManager\Types\FieldType;
use WordPressNoncesManager\Types\UrlType;
use WordPressNoncesManager\Verification\Verification;
use WordPressNoncesManager\WordPressNoncesManager;

class WordPressNoncesmanagerBuildTest extends AbstractTestCase
{
    /**
     * The Request Name.
     *
     * @var string
     **/
    public $request;

    /**
     * The Action.
     *
     * @var string
     **/
    public $action;

    /**
     * The Lifetime.
     *
     * @var int
     **/
    public $lifetime;

    /**
     * The Configuration.
     *
     * @var Configuration
     **/
    public $configuration;

    /**
     * The NonceManager
     *
     * @var NonceManager
     */
    private $nonceManager;

    /**
     * @var FieldType
     */
    private $fieldType;

    /**
     * @var UrlType
     */
    private $urlType;

    /**
     * @var Verifier
     */
    private $verifier;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->action = 'action';
        $this->request = 'request';
        $this->lifetime = 213;

        // The filter should be added once.
        Filters::expectAdded('nonce_life')->once();

        $this->configuration = new NonceConfiguration($this->action, $this->request, $this->lifetime);

        $this->nonceManager = WordPressNoncesManager::build($this->configuration);
    }

    /**
     * Test create a new Nonce Manager through the static create method and inject the required Dependencies
     *
     */
    public function testBuildedNonceManager(): void
    {
        self::assertInstanceOf(WordPressNoncesManager::class, $this->nonceManager);
    }

    /**
     * Test the builded Configuration
     *
     */
    public function testGetConfiguration(): void
    {
        self::assertInstanceOf(NonceConfiguration::class, $this->nonceManager->Configuration());
    }

    /**
     * Test the builded Nonce
     */
    public function testGetNonce(): void
    {
        self::assertInstanceOf(Nonce::class, $this->nonceManager->Nonce());
    }

    /**
     * Test the builded FieldType
     */
    public function testGetField(): void
    {
        self::assertInstanceOf(FieldType::class, $this->nonceManager->Field());
    }

    /**
     * Test the builded UrlType
     */
    public function testGetUrl(): void
    {
        self::assertInstanceOf(UrlType::class, $this->nonceManager->Url());
    }

    /**
     * Test the builded Verifier
     */
    public function testGetVerifier(): void
    {
        self::assertInstanceOf(Verification::class, $this->nonceManager->verify());
    }
}