<?php

namespace Tests\Unit;

use Tests\AbstractTestCase;
use WordPressNoncesManager\Configuration\NonceConfiguration;
use WordPressNoncesManager\Nonces\Nonce;

/**
 * Class NonceTest
 * @package Tests\Unit
 */
class NonceTest extends AbstractTestCase
{

    /**
     * The action.
     *
     * @var string
     **/
    public $action;

    /**
     * The request name.
     *
     * @var string
     **/
    public $request;

    /**
     * The lifetime.
     *
     * @var int
     **/
    public $lifetime;

    /**
     * The configuration.
     **/
    public $configuration;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     **/
    public function setUp(): void
    {
        parent::setUp();

        $this->action = 'action';
        $this->request = 'request';
        $this->lifetime = 213;
        $this->configuration = new NonceConfiguration($this->action, $this->request, $this->lifetime);
    }

    /**
     * Test Create new Nonce & get the new Nonce
     *
     */
    public function testCreateNonce(): void
    {
        $create = new Nonce($this->configuration);
        $create->create();
        $nonce = $create->getNonce();

        // Check if nonce is stored correctly.
        self::assertSame($nonce, $create->getNonce());
    }

    /**
     * Test the new created Nonce Magic Method __toString
     *
     */
    public function testMagicMethodToString() {
        $create = new Nonce($this->configuration);
        $create->create();
        $nonceToString = (string) $create;

        $nonce = $create->getNonce();

        self::assertSame($nonceToString, $nonce);
    }
}