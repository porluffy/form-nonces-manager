<?php

/**
 * Class FNM_Email_Subscription_Widget
 */
class FNM_Email_Subscription_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {

        add_action( 'wp',array( $this, 'subscribeEmailSend' ) );


        parent::__construct(
            'fnm_email_subscription_widget', // Base ID
            esc_html__( 'Email Subscription', 'text_domain' ), // Name
            array( 'description' => esc_html__( 'Email Subscription widget', 'text_domain' ), ) // Args
        );
    }

    public function subscribeEmailSend() {

        if ( isset($_POST['email']) && !empty($_POST['email']) ) {

            if ( fnm_nonce_verify('subscribe_email_action', 'subscribe_email_name') ) {

                $subscribe_email = sanitize_email($_POST['email']);
                $to = get_option('admil_email');
                $subject = 'Email Subscription';
                $body = "This '. $subscribe_email .' applied for email subscription.";
                $headers = array('Content-Type: text/html; charset=UTF-8');

                wp_mail( $to, $subject, $body, $headers );

                add_action('wp_footer', function(){
                    echo fnm_show_message("We got your email subscription.", 'success-msg');
                });

            } else {

                add_action('wp_footer', function(){
                    echo fnm_show_message("We could not be able to verify your action.", 'error-msg');
                });

            }

        }



    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        $url = "http" . (($_SERVER['SERVER_PORT'] == 443) ? "s" : "") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        ?>
        <div class="subscribe-box">

            <h3 class="title">Subscribe Our Blog</h3>

            <form action="<?php echo $url; ?>" method="post" class="fnm-form">
                <?php echo fnm_nonce_field('subscribe_email_action', 'subscribe_email_name'); ?>
                <div class="field-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="field-control" placeholder="john@mail.com">
                </div>

                <button type="submit" class="fnm-btn">Subscribe</button>

            </form>

        </div>

        <?php
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Email Subscription', 'text_domain' );
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

        return $instance;
    }

} // Email Subscription Widget class


/**
 * Email Subscription widget
 */
function register_email_subscription_widget() {
    register_widget( 'FNM_Email_Subscription_Widget' );
}
add_action( 'widgets_init', 'register_email_subscription_widget' );