<?php

/**
 * Show Message to the users
 *
 *
 * @param string $message
 * @param string|null $msg_class
 * @return string
 */
function fnm_show_message(string $message, string $msg_class = null) {

    return '<p class="fnm-msg '. $msg_class .'">'. $message .'</p>';
}




/*
 * Add styles for the plugins
 */
function demo_design_style() {
    wp_register_style( 'demo-styles',  plugin_dir_url( __FILE__ ) . 'css/style.css' );
    wp_enqueue_style( 'demo-styles' );
}
add_action('wp_enqueue_scripts', 'demo_design_style');