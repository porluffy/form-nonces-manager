![alt text](screenshot.jpeg)


##  About Form Nonce Manager Package 

**Form Nonce Manager** is composer package that could be able use on wordpress as a plugin. This package could help you to protect from outside data. For example, it would help us to protect every form fields data to be verified from our website.

##  Contents
- Features
- Requirements
- How to read this package
- Installation With Composer
- Manually Installation
- Helper Functions
- Using With WordPress Nonce Manager Class


## Features
- creating nonce 
- creating nonce field input
- verified nonce field
- demo working wordpress widget using this nonce package.


## Requirement
- PHP 7.2+
- composer
- WordPress 5.2.0


## How to read this package quickly
* To read this package, you have to download manually because repository is now in private. So you get source codes, you could publish it by yourself.
* Download it manually and `composer install`
* Put in wordpress plugin folder after `composer install`
* You could run `phpunit testing`
* This package have dependency injection depending on what we use class. So, you call a `class` you must do `dependency injection `
* `WordPresssNoncesManager Class` is the file you need to check to understand other `class`
* There is also a  `WordPressNoncesHelpers Class` .
* I have also created wordpress plugin activated file.
* You could also check demo folder. I have already created a wordpress widget named **subscribe-email-widget.php**


##  Installation With Composer
Install with [composer](https://getcomposer.org) is ready for now. But you could publish as a composer package by yourself if you have source codes. Here is how to [install with composer](https://www.w3resource.com/php/composer/create-publish-and-use-your-first-composer-package.php).


## Manually Installation
- Just download source code and unzip.
- Run composer install
- You could also directly test after composer install by activating as a wordpress plugin in plugin folder.


## Helper Functions
To use none function in form tag, just use below code

```

fnm_nonce_field(string $action, string $request_name);

//You would get link this input field
//<input type="hidden" id="subscribe_email_name" name="subscribe_email_name" value="262285b221">

```

To verify nonce fields, just use below code

```

fnm_nonce_verify(string $action, string $request_name);

/*
if ( fnm_nonce_verify('subscribe_email_action', 'subscribe_email_name') ) {

    //Do something when verification is correct

} else {

    //Do things when fail

}

*/

```


To generate nonce url link, just use below code

```

fnm_nonce_url(string $action, string $name, string $link);

//http://domain.com/?subscribe_email_name=23323232 // (23323232 that is nonce value)

```

** Helper Functions ** is cool and easy to use in everywhere. You could check the sample code in _Demofolder_ `subscribe-email-widget.php` file.



## Using With WordPress Nonce Manager Class
Some people love using **Class and method**. Here are a few step you need to follow to use **WordPress Nonces Manager Class**
- define configuration
- create nonce and use nonce

You could define live below code.

```
$configuration = new  NonceConfiguration('subscribe_email_action', 'subscribe_email_name', 30);
$wordpressNoncesManager = WordPressNoncesManager::build($configuration);

```


To generate a nonce value, use below code

```

$wordpressNoncesManager->nonce()->create();

echo $this->wordpressNoncesManager->nonce()->getNonce();

// you would get like this code 21343434234r

``` 

To generate a nonce url, use below code

```
$wordpressNoncesManager->url()->generate($link);

//you would get like this url http://domain.com/?request_name=23324324324 ( request_name=23324324324 is none name and value )

```


To generate a nonce input field

```
$wordpressNoncesManager->Field()->generate();

//<input type="hidden" id="subscribe_email_name" name="subscribe_email_name" value="262285b221">

```


To verify nonce field

```
$verification = $wordpressNoncesManager->verify();
echo  $verification->verify();

//You would get true or false 

```

